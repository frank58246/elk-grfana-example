﻿namespace Serilog.Example.Models
{
    public class GrafanaWebhookMessage
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string State { get; set; }
    }
}
