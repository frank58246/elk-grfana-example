﻿namespace Serilog.Example.Models
{
    public class TeamsWebhookMessage
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
}
