using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Serilog.Example
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
             Host.CreateDefaultBuilder(args)
                .UseSerilog((ctx, config) =>
                {

                    config.MinimumLevel.Information();
                    var option = new ElasticsearchSinkOptions(new Uri("http://elasticsearch:9200"))
                    {
                        AutoRegisterTemplate = true,
                        IndexFormat = $"log-example",
                        ModifyConnectionSettings = setting => setting.BasicAuthentication("elastic", "changeme")
                    };
                    config.WriteTo.Elasticsearch(option);



                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}
