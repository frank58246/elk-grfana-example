﻿using Serilog.Example.Models;
using System.IO;
using System.Net;
using System;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Serilog.Example.Utils
{
    public class Utility : IUtility
    {
        public bool SendTemasNotification(TeamsWebhookMessage teansMessage)
        {
            // 準備要 POST 的資料（這裡使用 JSON 格式作為範例）
            string postData = JsonConvert.SerializeObject(teansMessage);
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // 建立 WebRequest
            WebRequest request = WebRequest.Create(teansMessage.Url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            // 寫入 POST 資料到請求中
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            // 發送請求並取得回應
            WebResponse response = request.GetResponse();


            return ((HttpWebResponse)response).StatusCode == HttpStatusCode.OK;

        }
    }
}
