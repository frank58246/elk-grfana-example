﻿using Serilog.Example.Models;

namespace Serilog.Example.Utils
{
    public interface IUtility
    {
        public bool SendTemasNotification(TeamsWebhookMessage teamsMessage);
    }
}
