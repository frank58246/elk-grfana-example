﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog.Example.Models;
using Serilog.Example.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Serilog.Example.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class WebHookController : ControllerBase
    {

        private readonly ILogger<LogController> _logger;
        private readonly IUtility _utility;

        public WebHookController(ILogger<LogController> logger, IUtility utility)
        {
            _logger = logger;
            _utility = utility;
        }

        [HttpPost]
        public IActionResult Alert(GrafanaWebhookMessage reqest)
        {
            var teamsMessage = new TeamsWebhookMessage()
            {
                Title = reqest.Title,
                Text = reqest.Message,
                Url = "https://tutorabc.webhook.office.com/webhookb2/99b49727-a5b0-4212-8a3e-f96679bc99b5@75c7846f-00c3-4b3a-a7a5-3ef8419d43e0/IncomingWebhook/6dc3584e7e1e48fa8770d4af47ec3a1a/c5b0ce14-b99b-4e4f-b7f8-2c40ba1cd050"
            };
            var result = _utility.SendTemasNotification(teamsMessage);
            return Ok();
        }
    }


}
