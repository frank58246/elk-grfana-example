﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Serilog.Example.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class LogController : ControllerBase
    {

        private readonly ILogger<LogController> _logger;

        public LogController(ILogger<LogController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult InsertInfoLog()
        {
            _logger.LogInformation("info");
            return Ok();
        }

        [HttpGet]
        public IActionResult InsertErrorLog()
        {
            _logger.LogError("errpr");
            return Ok();
        }

 

    }


}
